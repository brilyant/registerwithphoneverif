import { takeLatest, all } from 'redux-saga/effects'
import Api from '../Services/Api' 
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux' 
import { RegisterTypes } from '../Redux/RegisterRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas' 


import { registerRequest, registerResendOtpRequest, registerVerifyOtpRequest } from './RegisterSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : Api.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    //REGISTER
    takeLatest(RegisterTypes.REGISTER_REQUEST, registerRequest, api),
    takeLatest(RegisterTypes.REGISTER_RESEND_OTP_REQUEST, registerResendOtpRequest, api),
    takeLatest(RegisterTypes.REGISTER_VERIFY_OTP_REQUEST, registerVerifyOtpRequest , api),
    
  ])
}
