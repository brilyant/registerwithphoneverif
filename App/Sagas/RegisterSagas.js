/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the Infinite Red Slack channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import RegisterActions from '../Redux/RegisterRedux'
import { Toast } from 'native-base'
// import { RegisterSelectors } from '../Redux/RegisterRedux'

export function * registerRequest (api, action) {
  const { data } = action
  const response = yield call(api.registerRequest, data)

  // success?
  if (response.ok) {
    yield put(RegisterActions.registerSuccess(response.data)) //saat action registerSuccess ini jalan, maka akan navigasi ke input otp, Lihat file ini lebih lanjut : App\Redux\NavigationRedux.js
  } else {
    yield put(RegisterActions.registerFailure())
  }
}

export function * registerResendOtpRequest (api, action) {
  const { data } = action
  const response = yield call(api.registerResendOtpRequest, data)
  console.log(response)
  // success?
  if (response.ok) {
    yield put(RegisterActions.registerResendOtpSuccess(response.data)) //saat action registerSuccess ini jalan, maka akan navigasi ke input otp, Lihat file ini lebih lanjut : App\Redux\NavigationRedux.js
  } else {
    yield put(RegisterActions.registerResendOtpFailure())
  }
}

export function * registerVerifyOtpRequest (api, action) {
  const { dataverify } = action
  const response = yield call(api.registerVerifyOtpRequest, dataverify)
  console.log(response)
  // success?
  if (response.ok) {
    Toast.show({
      text:response.data.message,
      type: response.data.isverified ? 'success' : 'danger'
    })

    if (response.data.isverified) {
      yield put(RegisterActions.registerVerifyOtpSuccess(response.data)) //saat action registerVerifyOtpSuccess ini jalan, maka akan navigasi ke awal screen
    } else {
      yield put(RegisterActions.registerVerifyOtpFailure())
    }
    
  } else {
    Toast.show({
      text: 'Terdapat Kesalahan Saat Melakukan Verifikasi',
      type: 'danger'
    })
    yield put(RegisterActions.registerVerifyOtpFailure())
  }
}
