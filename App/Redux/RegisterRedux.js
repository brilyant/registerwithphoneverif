import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  registerRequest: ['data'],
  registerSuccess: ['payload'],
  registerFailure: null,

  registerResendOtpRequest: ['data'],
  registerResendOtpSuccess: ['payload'],
  registerResendOtpFailure: null,

  registerVerifyOtpRequest: ['dataverify'],
  registerVerifyOtpSuccess: ['payloadverify'],
  registerVerifyOtpFailure: null,
 
  registerShowResendButton: null,
})

export const RegisterTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,

  dataverify: null,
  fetchingverify: null,
  payloadverify: null,
  errorverify: null,

  showResend: false,
})

/* ------------- Selectors ------------- */

export const RegisterSelectors = {
  getData: state => state.register.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, showResend: false })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload, showResend: false })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, showResend: false })

//VERIFY
export const requestverify = (state, { dataverify }) =>
  state.merge({ fetchingverify: true, dataverify, showResendverify: false })

// successful api lookup
export const successverify = (state, action) => {
  const { payloadverify } = action
  return state.merge({ fetchingverify: false, errorverify: null, payloadverify, showResendverify: false })
}

// Something went wrong somewhere.
export const failureverify = state =>
  state.merge({ fetchingverify: false, errorverify: true, showResendverify: false })


  
export const showresend = state =>
  state.merge({ showResend: true })  

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REGISTER_REQUEST]: request,
  [Types.REGISTER_SUCCESS]: success,
  [Types.REGISTER_FAILURE]: failure,

  //action registerResendOtpRequest, registerResendOtpSuccess, registerResendOtpFailure pakai reducer yg existing (request, success, failure) karena payloadnya sama saja dari server
  [Types.REGISTER_RESEND_OTP_REQUEST]: request,
  [Types.REGISTER_RESEND_OTP_SUCCESS]: success,
  [Types.REGISTER_RESEND_OTP_FAILURE]: failure,
  
  [Types.REGISTER_SHOW_RESEND_BUTTON]: showresend,
})
