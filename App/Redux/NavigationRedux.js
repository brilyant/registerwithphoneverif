import { StackActions, SwitchActions, NavigationActions } from 'react-navigation'
import AppNavigation from '../Navigation/AppNavigation'

const GO_TO_INPUT_OTP = (state) => {   
  const pushAction = StackActions.push({
    routeName: 'OtpVerificationScreen',
  }) 
  return AppNavigation.router.getStateForAction(pushAction, state) 
}  

const GOBACK = (state) => {   
  const popAction = StackActions.pop({
    n: 1,
  }) 
  return AppNavigation.router.getStateForAction(popAction, state) 
}  
 

export const reducer = (state, action) => {
  // console.log('action', action)
  switch (action.type) {
  case 'REGISTER_SUCCESS': //menangkap action REGISTER_SUCCESS (registerSuccess). bila action tersebut di jalankan, fungsi ini (GO_TO_INPUT_OTP) di jalankan
    return GO_TO_INPUT_OTP(state)  
  case 'REGISTER_VERIFY_OTP_SUCCESS': //bila action REGISTER_VERIFY_OTP_SUCCESS tertangkap jalankan GOBACK 
    return GOBACK(state)  
  }
  

  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
} 
 