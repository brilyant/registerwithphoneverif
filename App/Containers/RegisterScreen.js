import React, { Component } from 'react'
import { Text, Container, Header, Content, Form, Item, Input, Label, Left, Button, Body, Right, Icon, Title, Toast } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import RegisterActions from '../Redux/RegisterRedux'

// Styles
import styles from './Styles/RegisterScreenStyle'
import { ActivityIndicator } from 'react-native'

class RegisterScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      phone: ''
    }
  }
  render () {
    const {name, phone} = this.state
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack(null)}>
              <Icon name='ios-arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Registrasi</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Name</Label>
              <Input value={name} onChangeText={name => this.setState({name})} />
            </Item>
            <Item floatingLabel>
              <Label>Phone</Label>
              <Input value={phone} onChangeText={phone => this.setState({phone: phone.replace(/[^\d.-]/g, '')})} />
            </Item>
          </Form>
          <Button disabled={this.props.register.fetching === true} block style={{ marginHorizontal: 50, marginTop: 30}} onPress={() => {
            if (name && phone) {
              this.props.registerRequest({name,phone})
            } else {
              Toast.show({
                text: 'Harap isi kolom yg masih kosong',
                type: 'danger'
              })
            }
          }}>
            {
              (this.props.register.fetching === true ) ? 
                <ActivityIndicator size="small" />:
                <Text>Submit !</Text>
            }
          </Button>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    register: state.register
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerRequest: data => dispatch(RegisterActions.registerRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
