import React, { Component } from 'react'
import { Container, View, Header, Subtitle, Content, Button, Left, Right, Body, Icon, Text } from 'native-base'
import { connect } from 'react-redux'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import CountDown from 'react-native-countdown-component'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import RegisterActions from '../Redux/RegisterRedux'
    

// Styles
import styles from './Styles/OtpVerificationScreenStyle'
import { ActivityIndicator } from 'react-native'

class OtpVerificationScreen extends Component { 
  render () {
    const { payload, showResend } = this.props.register
    console.log(payload)
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Subtitle>Input PIN OTP</Subtitle>
          </Body>
          <Right />
        </Header>
        <View style={{ flex: 1, paddingHorizontal: 15 }}>
          <View style={{ flex: 1, padding: 15 }}>
            <Text style={{ textAlign: 'center'}}>{payload ? payload.message : ''}</Text>
            {
              (payload.fetchingverify === true) ? 
                <ActivityIndicator size="large" /> :
                <OTPInputView 
                  pinCount={6} 
                  codeInputFieldStyle={{ color: 'black' }} 
                  onCodeFilled={(code) => {
                    console.log(code)
                    this.props.registerVerifyOtpRequest({
                      name: payload.dataobject.name, 
                      otp: code
                    }) 
                  }}  
                />
            }
          </View>
          <View style={{ flex: 1, padding: 15 }}>
            <Text style={{ textAlign: 'center'}}>Kirim Ulang OTP Dalam :</Text>
            {
              (!showResend) ?
                <CountDown
                  //dokumentasi komponen ini di : https://github.com/talalmajali/react-native-countdown-component
                  key={payload.timetoresend+''} //key ini digunakan sebagai helper untuk mereset komponent countdown, saat kita resend otp, nilai timetoresend akan berubah di isi dr server, nah perubahan itu yg akan merefresh component ini agar melakukan countdown lg
                  until={payload ? (payload.timetoresend * 60) : 0} 
                  style={{ heigh: 20 }}
                  size={14} 
                  onFinish={() => {
                    this.props.registerShowResendButton() 
                  }}
                  digitStyle={{backgroundColor: '#FFF', borderWidth: 0, borderColor: 'transparent'}}
                  digitTxtStyle={{color: '#6FB0D8'}}
                  timeLabelStyle={{color: '#0275BB', fontWeight: 'bold'}}
                  timeToShow={['M', 'S']}
                  timeLabels={{m: 'Menit', s: 'Detik'}} 
                />
                : null
            }
            {
              (showResend === true) ?
                <Button small block onPress={() => {
                  this.props.registerResendOtpRequest({ name: payload.dataobject.name })
                }}>
                  <Text>
                    Kirim Ulang OTP
                  </Text>
                </Button>
                : null
            } 
          </View>
        </View> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    register: state.register
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerResendOtpRequest: data => dispatch(RegisterActions.registerResendOtpRequest(data)),
    registerShowResendButton: () => dispatch(RegisterActions.registerShowResendButton()),
    registerVerifyOtpRequest: (data) => dispatch(RegisterActions.registerVerifyOtpRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OtpVerificationScreen)
