import { createSwitchNavigator, createAppContainer } from 'react-navigation'   

import styles from './Styles/NavigationStyles'

import RegisterScreen from '../Containers/RegisterScreen'
import OtpVerificationScreen from '../Containers/OtpVerificationScreen'

// Manifest of possible screens
const RegisterSwitchNav = createSwitchNavigator({   
  RegisterScreen: { 
    screen: RegisterScreen  
  }, 
  OtpVerificationScreen: { 
    screen: OtpVerificationScreen  
  }, 
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'RegisterScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(RegisterSwitchNav)
