import { createAppContainer } from 'react-navigation'  
import RegisterScreen from '../Containers/RegisterScreen' 
import OtpVerificationScreen from '../Containers/OtpVerificationScreen' 
import { createStackNavigator } from 'react-navigation-stack';

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({   
  RegisterScreen: { screen: RegisterScreen }, 
  OtpVerificationScreen: { screen: OtpVerificationScreen }, 
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'RegisterScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})
  

export default createAppContainer(PrimaryNav)
